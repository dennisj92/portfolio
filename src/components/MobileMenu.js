import React from "react";
import { List, Drawer, Button, Icon } from "antd";
import { Link } from "gatsby"

export default class MobileMenu extends React.Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const data = [
      {
        title: 'Home',
        path: '/'
      },
      {
        title: 'Blog',
        path: '/blog'
      },
      {
        title: 'About',
        path: '/about'
      },
    ];

    return (
      <>
        <Button 
          aria-label="Open Menu"
          type="primary" 
          onClick={this.showDrawer}
          style={{
            margin: "1rem"
          }}
        >
          <Icon type="menu" />
          <span>
            Menu
          </span>
        </Button>
        <Drawer
          title="Menu"
          placement="right"
          closable={true}
          onClose={this.onClose}
          visible={this.state.visible}
        >
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={item => (
            <List.Item aria-label={item.title}>
              <List.Item.Meta
                title={
                  <Link href={item.path}>{item.title}</Link>
                }
              />
            </List.Item>
          )}
        />
        </Drawer>
      </>
    );
  }
}