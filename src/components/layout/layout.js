import React from 'react'
import 'antd/dist/antd.css'
import { Layout } from 'antd'
import Breakpoint, { BreakpointProvider } from 'react-socks'

import MobileMenu from '../MobileMenu'
import HeaderComponent from '../Header/Header'
import './layout.css'
import MainFooter from './../MainFooter/MainFooter';

const { Content } = Layout;

class LayoutComponent extends React.Component {
  render() {
    const { children } = this.props
    
    return (
      <BreakpointProvider>
        <Breakpoint large up>
          <HeaderComponent />
        </Breakpoint>
        <Breakpoint medium down>
          <MobileMenu />
        </Breakpoint>
        <Layout 
          className="layout"
          style={{
            background: `#fff` 
          }}
        >
          <Content className="center-main-content">
            <main>{children}</main>
          </Content>
          <MainFooter />
        </Layout>
      </BreakpointProvider>
    )
  }
}

export default LayoutComponent
