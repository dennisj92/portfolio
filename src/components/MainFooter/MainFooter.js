import React from 'react'
import { Layout } from 'antd'

const { Footer } = Layout;

const MainFooter = () => {
return(
  <Footer style={{
    textAlign: `center`,
    position: `fixed`,
    bottom: 0,
    right: 0,
    left: 0,
    background: `#fff`
  }}>
    <footer>
      © Dennis Johansson
    </footer>
  </Footer>
)
}

export default MainFooter