import { Link } from "gatsby"

import React from "react"
import { Typography, Menu } from 'antd';

const { Title } = Typography
export default class Sider extends React.Component {
  render() {
    return (
        <Menu
          mode="inline"
          style={{ 
            width: 256,
            position: "fixed",
            top: 0,
            bottom: 0
          }}
          title="Dennis Johansson"
        >
          <Menu.Item key="1">
            <Link to="/">
              Home
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="/blog">
              Blog
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to="/about">
              About
            </Link>
          </Menu.Item>
        </Menu>
    );
  }
}