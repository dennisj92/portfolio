import React from "react"
import { Typography } from 'antd'
import { Layout, Menu } from 'antd';
import { Link } from 'gatsby';
import './header.css'

const { Header } = Layout;
const { Title } = Typography

class HeaderComponent extends React.PureComponent {
  render() {
    return (
      <Header id="Main-header">
        <div className="inner-wrapper">
          <Title
            level={2}
          >
            Dennis Johansson
          </Title>
          <Menu
            mode="horizontal"
            style={{ lineHeight: '64px', display: "inline-block"}}
          >
            <Menu.Item key="1">
              <Link to="/">
                Home
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/blog">
                Blog
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/about">
                About
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Header>
    )
  }
}

export default HeaderComponent

