import React from "react"
import { graphql } from "gatsby"
import { Typography } from "antd"

import Layout from "../components/layout/layout"
import SEO from "../components/seo"

const { Title } = Typography

class AboutPage extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title

    return (
      <>
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title="About Page" />
        <Title>
          About
        </Title>
      </Layout>
      </>
    )
  }
}

export default AboutPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    markdownRemark {
      id
      excerpt(pruneLength: 180)
      html
      frontmatter {
        title
        description
      }
    }
  }
`
