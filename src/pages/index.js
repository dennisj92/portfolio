import React, { Component } from 'react'
import { graphql } from "gatsby"
import { Typography } from 'antd'
import Layout from '../components/layout/layout'
import SEO from "../components/seo"

// entry file (usually App.js or index.js)
export default class index extends Component {

  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const { Title } = Typography

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title="About Page" />
        <Title>
          Startpage
        </Title>
      </Layout>
    )
  }
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`