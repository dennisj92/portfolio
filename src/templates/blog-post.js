import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import { Typography, Breadcrumb, Button, Icon } from "antd"

const { Title, Paragraph } = Typography

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.markdownRemark
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
        />
        <Breadcrumb style={{
          paddingBottom: `15px`
        }}>
          <Breadcrumb.Item>
            <Link to="/blog">
              Blog
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>{post.frontmatter.title}</Breadcrumb.Item>
        </Breadcrumb>
        <Title level={2}>
          {post.frontmatter.title}
        </Title>
        <Paragraph>
          {post.frontmatter.date}
        </Paragraph>
        <Paragraph>
          <div dangerouslySetInnerHTML={{ __html: post.html }} />
        </Paragraph>
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
         <li>
            {previous && (
              <Button type="primary" aria-label={"Previous post: " + previous.frontmatter.title } >
                <Link to={previous.fields.slug} rel="prev">
                  <Icon type="left" />
                  <span style={{marginLeft: `5px`}}>
                    {previous.frontmatter.title}
                  </span>
                </Link>
              </Button>
            )}
          </li>
          <li>
            {next && (
              <Button type="primary" aria-label={"Next post: " + next.frontmatter.title }>
              <Link to={next.fields.slug} rel="next">
                <span style={{marginRight: `5px`}}>
                  {next.frontmatter.title}
                </span>
                <Icon type="right" />
              </Link>
              </Button>
            )}
          </li>
        </ul>
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 180)
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
      }
    }
  }
`
